<?php
/**
 * @file
 * uw_cfg_ist.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_cfg_ist_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
