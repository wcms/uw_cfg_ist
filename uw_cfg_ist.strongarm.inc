<?php
/**
 * @file
 * uw_cfg_ist.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_cfg_ist_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'cacheexclude_list';
  $strongarm->value = 'network-service-alerts
about/projects';
  $export['cacheexclude_list'] = $strongarm;

  return $export;
}
